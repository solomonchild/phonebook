#pragma once

#include <string>
#include <vector>
#include <exception>
#include <memory>

#include "PhoneRecord.hpp"

#include "PhoneStoreExceptions.hpp"

class PhoneRecordStore
{
public:
    PhoneRecordStore(const std::string& name = "phonebook");
    void write(const PhoneRecords& recs);
    PhoneRecords find(const PhoneRecord& like);
    PhoneRecords list();
    bool remove(const PhoneRecord&);
    bool edit(const PhoneRecord& old, const PhoneRecord& new_rec);

    ~PhoneRecordStore();
private:
    struct Impl;
    std::unique_ptr<Impl> impl_;
};
