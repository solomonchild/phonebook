#pragma once

#include <ostream>
#include <vector>

struct PhoneRecord
{
    std::string first_name;
    std::string last_name;
    std::string phone_no;
    std::string personal_id;

    bool operator==(const PhoneRecord& other) const
    {
        return first_name == other.first_name &&
                last_name == other.last_name &&
                phone_no == other.phone_no &&
                personal_id == other.personal_id;
    }

    static PhoneRecord null_record()
    {
        return {"", "", "" ,""};
    }

};
using PhoneRecords = std::vector<PhoneRecord>;

inline std::ostream& operator <<(std::ostream& os, const PhoneRecord& rec)
{
    os << rec.first_name << ", " << rec.last_name << ", " << rec.phone_no << ", " << rec.personal_id;
    return os;
}
