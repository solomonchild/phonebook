#pragma once

#include <exception>
#include <string>

class PhoneRecordStoreException : public std::exception
{
public:
    PhoneRecordStoreException(const std::string& what)
        : what_(what) { }
    std::string err() const
    {
        return what_;
    }

private:
    std::string what_;

};

class FindException : public PhoneRecordStoreException
{
public:
    FindException(const std::string& what)
        : PhoneRecordStoreException(what) { }

private:
    std::string what_;

};

class WriteException : public PhoneRecordStoreException
{
public:
    WriteException(const std::string& what)
        : PhoneRecordStoreException(what) { }

private:
    std::string what_;

};
