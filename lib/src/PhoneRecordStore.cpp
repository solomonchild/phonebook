#include "PhoneRecordStore.hpp"
#include <sqlite3.h>
#include <functional>

struct PhoneRecordStore::Impl
{

    static void asterisk_to_percent(PhoneRecord& rec)
    {
        auto replace = [](std::string str, const std::string& what, const std::string& with){
            std::string::size_type i = 0;
            while((i = str.find(what)) != std::string::npos)
            {
                str.replace(str.begin() + i, str.begin() + i + what.size(), with);
            }
            return str;
        };
        rec.first_name = replace(rec.first_name, "*", "%");
        rec.last_name = replace(rec.last_name, "*", "%");
        rec.phone_no = replace(rec.phone_no, "*", "%");
        rec.personal_id = replace(rec.personal_id, "*", "%");
    }

    sqlite3* hndl_;

    PhoneRecords list()
    {
        return find(PhoneRecord::null_record(), true);
    }

    bool edit(const PhoneRecord& old, const PhoneRecord& new_rec)
    {
        if(!remove(old))
        {
            return false;
        }
        write({new_rec});
		return true;
    }

    bool remove(const PhoneRecord& rec)
    {
        const std::string st = "DELETE FROM entries WHERE " + get_where_stmt(rec, false) + ";";
        char* err;
        auto ret = sqlite3_exec(hndl_, st.c_str(), nullptr, nullptr, &err);
        check_ret<WriteException>(ret, err);
        sqlite3_free(err);
		return true;
    }

    void write(const PhoneRecords& recs)
    {
        for(const auto& r : recs)
        {
            char* err = nullptr;
            auto st = "INSERT INTO entries values ('" + r.first_name + "','"
                         + r.last_name + "','"
                         + r.phone_no + "','"
                         + r.personal_id + "');";

            auto ret = sqlite3_exec(hndl_, st.c_str(), nullptr, nullptr, &err);
            check_ret<WriteException>(ret, err);
            sqlite3_free(err);
        }

    }
    std::string get_where_stmt(const PhoneRecord& like, bool empty_means_skip = true)
    {
        std::string stmt;
        bool has_one_clause = false;
        if(!like.first_name.empty() || !empty_means_skip)
        {
            stmt += "FNAME = '" + like.first_name + "'";
            has_one_clause = true;
        }
        if(!like.last_name.empty() || !empty_means_skip)
        {
            if(has_one_clause) stmt += " AND ";
            stmt += "LNAME = '" + like.last_name + "'";
            has_one_clause = true;
        }
        if(!like.phone_no.empty() || !empty_means_skip)
        {
            if(has_one_clause) stmt += " AND ";
            stmt += "PHONE_NO = '" + like.phone_no + "'";
            has_one_clause = true;
        }
        if(!like.personal_id.empty() || !empty_means_skip)
        {
            if(has_one_clause) stmt += " AND ";
            stmt += "PID = '" + like.personal_id + "'";
            has_one_clause = true;
        }
        return stmt;
    }
    std::string get_where_like_stmt(PhoneRecord like)
    {
        std::string stmt;
        asterisk_to_percent(like);
        bool has_one_clause = false;
        if(!like.first_name.empty())
        {
            stmt += "FNAME LIKE '" + like.first_name + "'";
            has_one_clause = true;
        }
        if(!like.last_name.empty())
        {
            if(has_one_clause) stmt += " AND ";
            stmt += "LNAME LIKE '" + like.last_name + "'";
            has_one_clause = true;
        }
        if(!like.phone_no.empty())
        {
            if(has_one_clause) stmt += " AND ";
            stmt += "PHONE_NO LIKE '" + like.phone_no + "'";
            has_one_clause = true;
        }
        if(!like.personal_id.empty())
        {
            if(has_one_clause) stmt += " AND ";
            stmt += "PID LIKE '" + like.personal_id + "'";
            has_one_clause = true;
        }
        return stmt;
    }

    PhoneRecords find(const PhoneRecord& like, bool empty_means_all = false)
    {
        std::string stmt;
        if(like == PhoneRecord::null_record())
        {
            if(empty_means_all)
            {
                stmt = "SELECT * FROM entries;";
            }
            else
            {
                return {};
            }
        }
        else
        {
            stmt = "SELECT * FROM entries WHERE ";
            stmt += get_where_like_stmt(like);
            stmt += ";";
        }

        char* err_msg = nullptr;
        PhoneRecords recs;

        std::function<void(const PhoneRecord&)> cbk = [&recs](const PhoneRecord& rec)
        {
            recs.push_back(rec);
        };
        auto ret = sqlite3_exec(hndl_, stmt.c_str(), phone_entry_cbk, &cbk, &err_msg);
        check_ret<FindException>(ret, err_msg);
        return recs;
    }

    template<typename E>
    static void check_ret(int ret, char* err_msg)
    {
        if(ret != SQLITE_OK)
        {
            std::string msg = err_msg;
            sqlite3_free(err_msg);
            throw E("sqlite3_exec failed with " + msg);
        }

    }

    static int phone_entry_cbk(void *cbk, int argc, char **argv, char **azColName)
    {
        PhoneRecord entry;
        for(auto i = 0; i < argc; i++)
        {
            std::string val = argv[i];
            std::string col = azColName[i];
            if(col == "FNAME")
            {
                entry.first_name = val;
            }
            else if(col == "LNAME")
            {
                entry.last_name = val;
            }
            else if(col == "PHONE_NO")
            {
                entry.phone_no = val;
            }
            else if(col == "PID")
            {
                entry.personal_id = val;
            }
        }
        auto fn = (std::function<void(const PhoneRecord&)>*)cbk;
        (*fn)(entry);
        return 0;
    }
    static int has_table(void *cbc, int argc, char **argv, char **azColName)
    {
        for(auto i = 0; i < argc; i++)
        {
            std::string val = argv[i];
            if(val == "entries")
            {
                auto fn = (std::function<void(void)>*)cbc;
                (*fn)();
            }
        }
        return 0;
    }

    bool table_exist(const std::string& name)
    {
        auto stmt = "SELECT name FROM sqlite_master\
        WHERE name='" + name + "';";
        bool has_t = false;
        std::function<void(void)> cbc = [&has_t]()
        {
            has_t = true;
        };
        auto ret = sqlite3_exec(hndl_, stmt.c_str(), has_table, &cbc, nullptr);

        return has_t;
    }

    Impl(const std::string& name)
    {
        int ret;
        if((ret = sqlite3_open(name.c_str(), &hndl_)) != SQLITE_OK)
        {
            throw PhoneRecordStoreException("sqlite3_open failed with " + std::to_string(ret) + ", " + sqlite3_errmsg(hndl_));
        }
        if(!table_exist("entries"))
        {
            char* err = nullptr;
            ret = sqlite3_exec(hndl_, "CREATE TABLE entries(FNAME TEXT, LNAME TEXT, PHONE_NO TEXT, PID TEXT PRIMARY KEY);", nullptr, nullptr, &err);
            check_ret<PhoneRecordStoreException>(ret, err);
        }
    }

    ~Impl()
    {
        sqlite3_close(hndl_);
    }

};

PhoneRecordStore::~PhoneRecordStore() = default;

void PhoneRecordStore::write(const PhoneRecords& recs)
{
    impl_->write(recs);
}

PhoneRecords PhoneRecordStore::find(const PhoneRecord& like)
{
    return impl_->find(like);
}

bool PhoneRecordStore::remove(const PhoneRecord& like)
{
    return impl_->remove(like);
}

bool PhoneRecordStore::edit(const PhoneRecord& old, const PhoneRecord& new_rec)
{
    return impl_->edit(old, new_rec);
}
PhoneRecords PhoneRecordStore::list()
{
    return impl_->list();

}
PhoneRecordStore::PhoneRecordStore(const std::string& name)
    : impl_(new Impl(name))
{
}
