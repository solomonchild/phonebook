## Building
```bash
mkdir bld
cd bld
cmake -Dgtest_disable_pthreads=1 -G <GENERATOR> ../
make
```
If building through a Qt Creator, `gtest_disable_pthreads` can be "ticked" in Projects-Build. Build Settings will contain CMake variables for configuring gtest before building. 