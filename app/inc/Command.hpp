#pragma once

#include <memory>
#include <string>

#include "PhoneRecord.hpp"

struct Menu;
struct Command : public std::enable_shared_from_this<Command>
{

    Command(const std::string& name)
        : name(name)
    {
    }
    virtual ~Command() {}

    virtual std::string description() { return name; }

    virtual void run() {};

protected:
    friend struct Menu;
    std::string name;
    std::weak_ptr<Menu> parent_;
};

struct ListCommand : public Command
{
    ListCommand(std::shared_ptr<Menu> parent = nullptr);
    virtual void run() override;
protected:
    std::shared_ptr<Menu> menu_;
};

struct ExitCommand : public Command
{
    ExitCommand()
        :Command("Exit")
    {
    }

    void run() override;
};

struct AddCommand : public Command
{
    AddCommand(std::shared_ptr<Menu> parent = nullptr)
    :Command("Add")
    {
        parent_ = parent;
    }
    void run() override;
};

struct EditCommand : public Command
{
    EditCommand(std::weak_ptr<Menu> parent)
    :Command("Edit")
    {
        parent_ = parent;
    }
    void run() override;
    void set_rec(const PhoneRecord& rec) { rec_ = rec; }
protected:
    PhoneRecord rec_;
};

struct DeleteCommand : public Command
{
    DeleteCommand(std::weak_ptr<Menu> parent)
    :Command("Edit")
    {
        parent_ = parent;
    }
    void set_rec(const PhoneRecord& rec) { rec_ = rec; }
    void run() override;
protected:
    PhoneRecord rec_;
};

struct TransitionCommand : public Command
{
    TransitionCommand(const std::string& name, std::shared_ptr<Menu> menu, std::shared_ptr<Menu> parent = nullptr);
    void run() override;
protected:
    std::shared_ptr<Menu> menu_;
};

struct BackCommand : public Command
{
    BackCommand(std::weak_ptr<Menu> parent)
    :Command("Back")
    {
        parent_ = parent;
    }
    void run() override;
};

struct FindCommand : public Command
{
    enum class Criteria
    {
        name,
        last_name,
        phone_no,
        pid,
        _end
    };
    FindCommand(Criteria crit, const std::string& name, std::shared_ptr<Menu> parent)
    :Command("Find by " + name)
    {
        parent_ = parent;
        crit_ = crit;
    }
    void run() override;
protected:
    Criteria crit_;
};
