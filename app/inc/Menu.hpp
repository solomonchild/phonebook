#pragma once
#include <memory>
#include <vector>
#include <iostream>

#include "PhoneRecord.hpp"
#include "PhoneRecordStore.hpp"
#include "IoUtils.hpp"
#include "Command.hpp"

inline void show(int entry, const std::string& name)
{
    std::cout << entry << ")" << name << std::endl;
}

struct Menu : public std::enable_shared_from_this<Menu>
{
    virtual void init()
    {
        if(initialized)
            return;

        initialized = true;
        for(const auto& menu : sub_menus_)
        {
            menu->parent_ = shared_from_this();
        }
        auto pptr = parent_.lock();
        if(!pptr)
            sub_menus_.insert(sub_menus_.begin(), 1U, std::shared_ptr<Command>(new ExitCommand));
        else
            sub_menus_.insert(sub_menus_.begin(), 1U, std::shared_ptr<Command>(new BackCommand(pptr)));
    }

    Menu(std::vector<std::shared_ptr<Command>>&& sub_menus = {}, std::shared_ptr<Menu> parent = nullptr)
        : sub_menus_(std::move(sub_menus)),
          parent_(parent)

    {
    }
    Menu(std::shared_ptr<Menu> parent = nullptr)
          : parent_(parent)

    {
    }

    virtual ~Menu() {}

    void print_commands()
    {
        for(auto i = 0; i < sub_menus_.size(); i++)
        {
            ::show(i, sub_menus_[i]->description());
        }
    }
    virtual void show()
    {
        init();
        for(;;)
        {
            system("cls");
            print_commands();
            int choice = Printer::get_int("Choose an option: ");

            if(choice >= sub_menus_.size())
                continue;
            sub_menus_[choice]->run();
        }
    }

protected:
    friend class TransitionCommand;
    bool initialized = false;
    std::vector<std::shared_ptr<Command>> sub_menus_;
    std::weak_ptr<Menu> parent_;
};

struct EditMenu : public Menu
{
    EditMenu(std::shared_ptr<Menu> parent)
    : Menu(parent)
    {
    }

    void show() override
    {
        init();
        for(;;)
        {
            system("cls");
            PhoneRecordStore store;
            auto recs = store.list();
            Printer::print_recs(recs);

            int choice = Printer::get_int("Select an entry to edit or enter 0 to go back: ");
            if(choice == 0)
            {
                sub_menus_[0]->run();
            }
            else if(--choice < recs.size())
            {
                auto rec = recs[choice];
                if(!cmd_)
                {
                    cmd_ = std::make_shared<EditCommand>(shared_from_this());
                }
                cmd_->set_rec(rec);
                cmd_->run();
            }
        }
    }
    std::shared_ptr<EditCommand> cmd_;

};

struct FindMenu : public Menu
{
    FindMenu(std::shared_ptr<Menu> parent = nullptr)
    : Menu(parent)
    {
    }

    void init() override
    {
        if(initialized)
            return;
        sub_menus_.push_back(std::make_shared<FindCommand>(FindCommand::Criteria::name, "name", shared_from_this()));
        sub_menus_.push_back(std::make_shared<FindCommand>(FindCommand::Criteria::last_name, "last name", shared_from_this()));
        sub_menus_.push_back(std::make_shared<FindCommand>(FindCommand::Criteria::phone_no, "phone #", shared_from_this()));
        sub_menus_.push_back(std::make_shared<FindCommand>(FindCommand::Criteria::pid, "pid", shared_from_this()));
        Menu::init();
    }

    void show() override
    {
        init();
        for(;;)
        {
            system("cls");

            print_commands();
            int choice = Printer::get_int("Choose an option: ");
            if(choice < sub_menus_.size())
            {
                sub_menus_[choice]->run();
            }
            else
                continue;
        }
    }

};

struct DeleteMenu : public Menu
{
    DeleteMenu(std::shared_ptr<Menu> parent)
    : Menu(parent)
    {
    }

    void show() override
    {
        init();
        for(;;)
        {
            system("cls");
            PhoneRecordStore store;
            auto recs = store.list();
            Printer::print_recs(recs);

            int choice = Printer::get_int("Select an entry to delete or enter 0 to go back: ");
            if(choice == 0)
            {
                sub_menus_[0]->run();
            }
            else if(--choice < recs.size())
            {
                auto rec = recs[choice];
                if(!cmd_)
                {
                    cmd_ = std::make_shared<DeleteCommand>(shared_from_this());
                }
                cmd_->set_rec(rec);
                cmd_->run();
            }
        }
    }
    std::shared_ptr<DeleteCommand> cmd_;

};

struct ListMenu : public Menu
{
    ~ListMenu() override
    {
        std::cout << "DESTR" << std::endl;
    }

    ListMenu(std::shared_ptr<Menu> parent)
    : Menu(parent)
    {
    }

    void init() override
    {
        if(initialized)
            return;
        sub_menus_.push_back(std::make_shared<TransitionCommand>("Edit", std::make_shared<EditMenu>(shared_from_this())));
        sub_menus_.push_back(std::make_shared<TransitionCommand>("Delete", std::make_shared<DeleteMenu>(shared_from_this())));
        Menu::init();
    }

    void show() override
    {
        init();
        for(;;)
        {
            system("cls");
            PhoneRecordStore store;
            auto recs = store.list();
            Printer::print_recs(recs);
            print_commands();

            int choice = Printer::get_int("Choose an option: ");
            if(choice >= sub_menus_.size())
                continue;
            sub_menus_[choice]->run();
        }
    }
};



