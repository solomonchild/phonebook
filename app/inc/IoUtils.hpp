#pragma once
#include <PhoneRecord.hpp>

#include <array>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>

namespace Printer
{

static std::string get_string(const std::string& prompt = "")
{
    std::cout << prompt;
    std::string _;
    std::getline(std::cin, _);
    return _;
}

static int get_int(const std::string& prompt = "")
{
    int _;
    std::cout << prompt;
    while(!(std::cin >> _))
    {
        std::cout << "Invalid input. Try Again." << std::endl;
        std::cin.clear();
        std::cin.ignore();
        std::cout << prompt;
    }
    //Enter key
    std::cin.ignore();
    return _;
}

static void press_key()
{
    std::cout << "Press any key to continue..." << std::endl;
    std::string _;
    std::getline(std::cin, _);
}

static PhoneRecord edit_rec(const PhoneRecord& to_edit)
{
    PhoneRecord rec = to_edit;
    std::string buffer;
    std::cout << "Press enter to leave intact\n";
    std::cout << "New first name (" << to_edit.first_name << "): ";
    std::getline(std::cin, buffer);
    if(!buffer.empty())
        rec.first_name = buffer;
    std::cout << "New last name (" << to_edit.last_name << "): ";
    std::getline(std::cin, buffer);
    if(!buffer.empty())
        rec.last_name = buffer;
    std::cout << "New phone no (" << to_edit.phone_no << "): ";
    std::getline(std::cin, buffer);
    if(!buffer.empty())
        rec.phone_no = buffer;
    std::cout << "New personal ID (" << to_edit.personal_id << "): ";
    std::getline(std::cin, buffer);
    if(!buffer.empty())
        rec.personal_id = buffer;
    return rec;
}

static PhoneRecord get_rec()
{
    std::string buffer;
    std::cout << "Press enter to leave intact\n";
    PhoneRecord rec;
    std::cout << "First name: ";
    std::getline(std::cin, buffer);
    if(!buffer.empty())
        rec.first_name = buffer;
    std::cout << "Last name: ";
    std::getline(std::cin, buffer);
    if(!buffer.empty())
        rec.last_name = buffer;
    std::cout << "Phone no: ";
    std::getline(std::cin, buffer);
    if(!buffer.empty())
        rec.phone_no = buffer;
    std::cout << "Personal ID: ";
    std::getline(std::cin, buffer);
    if(!buffer.empty())
        rec.personal_id = buffer;
    return rec;
}
static void print_recs (const PhoneRecords& recs)
{
    std::array<std::string, 5> headings = {
        "No", "First Name", "Last Name", "Phone #", "PID"
    };
    const auto longest_name_it = std::max_element(recs.begin(), recs.end(), [](const  PhoneRecord& r, const PhoneRecord& r2){return r.first_name.size() < r2.first_name.size();});
    const auto longest_surname_it = std::max_element(recs.begin(), recs.end(), [](const  PhoneRecord& r, const PhoneRecord& r2){return r.last_name.size() < r2.last_name.size();});
    const auto longest_phone_no_it = std::max_element(recs.begin(), recs.end(), [](const  PhoneRecord& r, const PhoneRecord& r2){return r.phone_no.size() < r2.phone_no.size();});
    const auto longest_pid_it = std::max_element(recs.begin(), recs.end(), [](const  PhoneRecord& r, const PhoneRecord& r2){return r.personal_id.size() < r2.personal_id.size();});
    const std::array<size_t, 5> lens_of_fields =
    {
        std::max(size_t(log10(recs.size())) + 1, headings[0].size()),
        std::max((longest_name_it == recs.end()) ? 0 : longest_name_it->first_name.size(), headings[1].size()),
        std::max((longest_surname_it == recs.end())? 0 : longest_surname_it->last_name.size(), headings[2].size()),
        std::max((longest_phone_no_it == recs.end()) ? 0 : longest_phone_no_it->phone_no.size(), headings[3].size()),
        std::max((longest_pid_it == recs.end()) ? 0 : longest_pid_it->personal_id.size(), headings[4].size())
    };

    auto print = [](const auto& field, const auto len_of_field, size_t max_len_for_field){
        std::ostringstream oss;
        std::ostreambuf_iterator<char> ss(oss);
        oss << "|" << field;
        std::fill_n(ss, max_len_for_field - len_of_field, ' ');
        auto ret = oss.str().size();
        std::cout << oss.str();
        return ret;
    };

    auto print_str = [](const std::string& field, size_t max_len_for_field){
        std::ostringstream oss;
        std::ostreambuf_iterator<char> ss(oss);
        oss << "|" << field;
        std::fill_n(ss, max_len_for_field - field.size(), ' ');
        auto ret = oss.str().size();
        std::cout << oss.str();
        return ret;
    };
    auto end_line = [](){
        std::cout << "|" << std::endl;
        return 1;
    };
    auto print_delim_line = [](size_t len)
    {
        std::ostreambuf_iterator<char> ss(std::cout);
        std::cout << "|";
        std::fill_n(ss, len-2, '-');
        std::cout << "|" << std::endl;

    };
    auto len = 0u;
    for(auto i = 0u; i < headings.size(); i++)
    {
        len += print_str(headings[i], lens_of_fields[i]);
    }
    len += end_line();
    print_delim_line(len);

    auto index = 1U;
    for(const auto& r : recs)
    {
        auto len = print(index++, log10(index) + 1, lens_of_fields[0]);
        len += print_str(r.first_name, lens_of_fields[1]);
        len += print_str(r.last_name, lens_of_fields[2]);
        len += print_str(r.phone_no, lens_of_fields[3]);
        len += print_str(r.personal_id, lens_of_fields[4]);
        len += end_line();

        print_delim_line(len);
    }
}
}
