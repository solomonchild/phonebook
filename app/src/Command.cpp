#include "Command.hpp"
#include "Menu.hpp"

void ExitCommand::run()
{
    exit(0);
}

void BackCommand::run()
{
    if(auto ptr = parent_.lock())
    {
        ptr->show();
    }
}

void ListCommand::run()
{
    if(!menu_)
    {
        menu_ = std::make_shared<ListMenu>(parent_.lock());
    }
    menu_->show();
}

TransitionCommand::TransitionCommand(const std::string& name, std::shared_ptr<Menu> menu, std::shared_ptr<Menu> parent)
:Command(name)
{
    parent_ = parent;
    menu_ = menu;
}

void TransitionCommand::run()
{
    if(!menu_->parent_.lock())
    {
        menu_->parent_ = parent_;
    }
    menu_->show();
}

void EditCommand::run()
{
    auto new_rec = Printer::edit_rec(rec_);
    PhoneRecordStore store;
    store.edit(rec_, new_rec);
}

void AddCommand::run()
{
    auto rec = Printer::get_rec();
    PhoneRecordStore store;
    store.write({rec});
    std::cout << "\nAdded the following record" << std::endl;
    Printer::print_recs({rec});
    Printer::press_key();
}

void DeleteCommand::run()
{
    PhoneRecordStore store;
    store.remove(rec_);
}

ListCommand::ListCommand(std::shared_ptr<Menu> parent)
        : Command("List")
{

    parent_ = parent;
}

void FindCommand::run()
{
    PhoneRecord rec;
    std::cout << "Use * for fuzzy searching. E.g. Jo* and *hn both find John" << std::endl;
    switch(crit_)
    {
        case Criteria::name: rec.first_name = Printer::get_string("Enter a name: "); break;
        case Criteria::last_name: rec.last_name = Printer::get_string("Enter a last name: "); break;
        case Criteria::phone_no: rec.phone_no = Printer::get_string("Enter a phone #: "); break;
        case Criteria::pid: rec.personal_id = Printer::get_string("Enter a personal ID: "); break;
    }
    PhoneRecordStore store;
    auto recs = store.find(rec);
    std::cout << "Found " << recs.size() << " records." << std::endl;
    Printer::print_recs(recs);
    Printer::press_key();
}
