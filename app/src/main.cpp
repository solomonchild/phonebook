#include "PhoneRecordStore.hpp"
#include "PhoneStoreExceptions.hpp"


#include "IoUtils.hpp"
#include "Menu.hpp"

int main()
{
    std::vector<std::shared_ptr<Command>> menus {
        std::make_shared<ListCommand>(),
        std::make_shared<AddCommand>(),
        std::make_shared<TransitionCommand>("Find", std::make_shared<FindMenu>(nullptr))
    };

    auto menu = std::make_shared<Menu>(std::move(menus));
    for(;;)
    {
        try
        {
            menu->show();
        }
        catch(PhoneRecordStoreException& e)
        {
            std::cerr << e.err() << std::endl;
            Printer::press_key();
        }
    }
}
