#include <gtest/gtest.h>
#include "PhoneRecordStore.hpp"

#include <sqlite3.h>
#include <functional>

static int phone_entry_cbk(void *cbk, int argc, char **argv, char **azColName)
{
    PhoneRecord entry;
    for(auto i = 0; i < argc; i++)
    {
        std::string val = argv[i];
        std::string col = azColName[i];
        if(col == "FNAME")
        {
            entry.first_name = val;
        }
        else if(col == "LNAME")
        {
            entry.last_name = val;
        }
        else if(col == "PHONE_NO")
        {
            entry.phone_no = val;
        }
        else if(col == "PID")
        {
            entry.personal_id = val;
        }
    }
    auto fn = (std::function<void(const PhoneRecord&)>*)cbk;
    (*fn)(entry);
    return 0;
}
class PhoneRecordStoreTest  : public testing::Test
{
protected:
    sqlite3* hdl_ = nullptr;
public:
    void SetUp()
    {
        sqlite3_enable_shared_cache(1);
        auto ret = sqlite3_open(name.c_str(), &hdl_);
        ASSERT_EQ(SQLITE_OK, ret);

        ret = sqlite3_exec(hdl_, "DROP TABLE IF EXISTS 'entries';", nullptr, nullptr, nullptr);
    }
    void TearDown()
    {
        if(hdl_ != nullptr)
            sqlite3_close(hdl_);
    }
    const std::string name = "db_file";
};

TEST_F(PhoneRecordStoreTest, FindInEmpty)
{
    PhoneRecordStore store(name);

    auto found_recs = store.find(PhoneRecord{"John", "", "", ""});
    ASSERT_EQ((PhoneRecords{}), found_recs);
}

TEST_F(PhoneRecordStoreTest, FindEmpty)
{
    PhoneRecordStore store(name);

    auto found_recs = store.find(PhoneRecord{"", "", "", ""});
    ASSERT_EQ((PhoneRecords{}), found_recs);
}

TEST_F(PhoneRecordStoreTest, FindTest)
{
    PhoneRecordStore store(name);

    static PhoneRecords expected = {
        {"John", "Smith", "120-100-289", "1400-124"},
        {"James", "Watts", "111-089-239", "1411-132"},
    };
    for(const auto& rec : expected)
    {
        std::string stmt = "INSERT INTO entries VALUES ('" +rec.first_name + "','" +
                     rec.last_name + "','" +
                     rec.phone_no + "','" +
                     rec.personal_id + "');";
        char* err_msg;
        auto ret = sqlite3_exec(hdl_, stmt.c_str(), nullptr, nullptr, &err_msg);
        ASSERT_EQ(SQLITE_OK, ret);
    }

    for(const auto& rec : expected)
    {
        auto found_rec = store.find(PhoneRecord{rec.first_name, "", "", ""});
        ASSERT_EQ(1, found_rec.size());
        ASSERT_EQ(rec, found_rec[0]);
        found_rec = store.find(PhoneRecord{"", rec.last_name, "", ""});
        ASSERT_EQ(1, found_rec.size());
        ASSERT_EQ(rec, found_rec[0]);
        found_rec = store.find(PhoneRecord{"", "", rec.phone_no, ""});
        ASSERT_EQ(1, found_rec.size());
        ASSERT_EQ(rec, found_rec[0]);
        found_rec = store.find(PhoneRecord{"", "", "", rec.personal_id});
        ASSERT_EQ(1, found_rec.size());
        ASSERT_EQ(rec, found_rec[0]);
    }
}

TEST_F(PhoneRecordStoreTest, WriteTest)
{
    PhoneRecordStore store(name);
    static PhoneRecords expected = {
        {"John", "Smith", "120-100-289", "1400-124"},
        {"James", "Watts", "111-089-239", "1411-132"},
    };
    store.write(expected);
    static int current_idx = 0;
    std::function<void(const PhoneRecord&)> cbk = [](const PhoneRecord& rec)
    {
        ASSERT_LE(current_idx, 1);
        ASSERT_EQ((expected[current_idx++]), rec);
    };
    sqlite3_exec(hdl_, "SELECT * FROM entries;", phone_entry_cbk, &cbk, nullptr);
    ASSERT_EQ(current_idx, 2);
}

TEST_F(PhoneRecordStoreTest, WriteDuplicateTest)
{
    PhoneRecordStore store(name);
    static PhoneRecords expected = {
        {"John", "Smith", "120-100-289", "1400-124"},
        {"James", "Watts", "111-089-239", "1400-124"},
    };
    ASSERT_THROW(store.write(expected), WriteException);
}


TEST_F(PhoneRecordStoreTest, RemoveTest)
{
    PhoneRecordStore store(name);
    static PhoneRecords to_write = {
        {"John", "Smith", "120-100-289", "1400-124"},
        {"James", "Watts", "111-089-239", "1400-125"},
    };
    store.write(to_write);
    ASSERT_TRUE(store.remove(to_write[1]));
    static int current_idx = 0;
    std::function<void(const PhoneRecord&)> cbk = [](const PhoneRecord& rec)
    {
        ASSERT_LE(current_idx, 1);
        ASSERT_EQ((to_write[current_idx++]), rec);
    };
    sqlite3_exec(hdl_, "SELECT * FROM entries;", phone_entry_cbk, &cbk, nullptr);
    ASSERT_EQ(current_idx, 1);

}

TEST_F(PhoneRecordStoreTest, EditTest)
{
    PhoneRecordStore store(name);
    static PhoneRecords to_write = {
        {"John", "Smith", "120-100-289", "1400-124"},
        {"James", "Watts", "111-089-239", "1400-125"},
    };
    static PhoneRecords expected = {
        {"John", "Smith", "120-100-289", "1400-124"},
        {"Ivan", "Watts", "111-089-239", "2400-125"},
    };
    store.write(to_write);

    ASSERT_TRUE(store.edit(to_write[1], expected[1]));
    static int current_idx = 0;
    std::function<void(const PhoneRecord&)> cbk = [](const PhoneRecord& rec)
    {
        ASSERT_LE(current_idx, 1);
        ASSERT_EQ((expected[current_idx++]), rec);
    };
    sqlite3_exec(hdl_, "SELECT * FROM entries;", phone_entry_cbk, &cbk, nullptr);
    ASSERT_EQ(current_idx, 2);

}

TEST_F(PhoneRecordStoreTest, ListTest)
{
    PhoneRecordStore store(name);
    static PhoneRecords expected = {
        {"John", "Smith", "120-100-289", "1400-124"},
        {"James", "Watts", "111-089-239", "1400-125"},
        {"James", "Cavendish", "111-089-239", "1401-125"},
    };
    store.write(expected);

    auto recs = store.list();
    ASSERT_EQ(expected, recs);

}
